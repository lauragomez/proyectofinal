Previamente se debe crear la base de datos

1. Crear carpeta y dentro de ella ejecutar el comando npm init. Seguir las indicaciones del comando.
2. Instalar las dependencias del proyecto:
   mongoose, body-parser, express
3. Instalar las dependencias de desarrollo:
   nodemon
4. En package.json:
   "start": "nodemon index.js"
5. crear el archivo index.js

let mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/eam')
.then(() => {
console.log('Conexión correcta con mongodb')
}).catch(err => console.log(err));

6. ejecutar en la terminal el comando mongod, para levantar el servicio de mongodb
7. Crear la base de datos
8. ejecutar el comando npm run start para probar el correcto funcionamiento

En este punto debe aparecer en la terminal el mensaje definido en index.js

---**\*\***\*\*\*\***\*\***

9. Agregar a index.js

let mongoose = require('mongoose');
let app = require('./app');
let port = 3000;

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/eam')
.then(() => {
app.listen(port, () => console.log('Corriendo en el puerto 3000'))
}).catch(err => console.log(err));

10. crear el archivo app.js

let express = require('express');

let app = express();

app.use('/', (req, res) => {
res.status(200).send('<h1>Hola mundo</h1>')
});

module.exports = app;

11. Verfiicar el funcionamiento en la consola y en la url

12. Probar salida en formato json. Modificar app.js

app.use('/', (req, res) => {
res.status(200).json({
message: "OK"
})
});

---**\*\***\*\*\*\***\*\***

13. Crear estructura con el patron MVC
14. Modificar app.js

let express = require('express');
let bodyParser = require('body-parser');

let app = express();

let router = require('./routes/index');

app.use(bodyParser.urlencoded({
extended: false
}));
app.use(bodyParser.json());

app.use('/', router);

module.exports = app;

15. crear modelo:

let moongoose = require('mongoose');
let Schema = moongoose.Schema;

let PlaceSchema = Schema({
name: String,
description: String
});

module.exports = moongoose.model('Place', PlaceSchema);

16. Crear controlador

let Place = require('../models/Place')

let PlaceController = {
show(req, res) {
return res.status(200).json({
message: 'OK'
});
}
}

module.exports = PlaceController;

17. Crear carpeta routes, archivo index.js

let express = require('express');
let PlaceController = require('../controllers/PlaceController');

let router = express.Router();

router.get('/places', PlaceController.show);

module.exports = router;

18. Verificar el funcionamiento en la consola y en la url /places, debe mostrar el mensaje requerido

19. Agregar método en el controlador para crear documentos(registros):

    store(req, res) {
    Place.create({
    name: req.body.name,
    description: req.body.description
    }).then(doc => {
    res.json(doc)
    }).catch(err => {
    res.json(err)
    })
    }

20. Crear ruta
    router.post('/places', PlaceController.store);

21. Instalar postman (cliente rest)

22. Probar servicio POST '/places' en postman
    http://localhost:3000/places

23. Modificar método show en el controlador para consultar los documentos de la colección places:
    show(req, res) {
    Place.find({})
    .then(docs => {
    res.json(docs)
    }).catch(err => {
    res.json(err)
    })
    }

24. Crear método update para modificar datos en el controlador
