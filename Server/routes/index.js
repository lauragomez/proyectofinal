let express = require('express');
let PlaceController = require('../controllers/PlaceController');
let UserController = require('../controllers/UserController');

let router = express.Router();

router.get('/places', PlaceController.show);

router.post('/places', PlaceController.store);

router.get('/places/:id', PlaceController.find);

router.post('/places/:id', PlaceController.destroy);

router.get('/users',UserController.show);

router.get('/users/:id',UserController.find);

router.post('/users', UserController.store);

module.exports = router;
